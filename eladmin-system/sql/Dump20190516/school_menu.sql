-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: school
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `i_frame` bit(1) DEFAULT NULL COMMENT '是否外链',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `pid` bigint(20) NOT NULL COMMENT '上级菜单ID',
  `sort` bigint(20) NOT NULL COMMENT '排序',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `path` varchar(255) DEFAULT NULL COMMENT '链接地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'2018-12-18 15:11:29',_binary '\0','系统管理',NULL,0,1,'system','system'),(2,'2018-12-18 15:14:44',_binary '\0','用户管理','system/user/index',1,2,'peoples','user'),(3,'2018-12-18 15:16:07',_binary '\0','角色管理','system/role/index',1,3,'role','role'),(4,'2018-12-18 15:16:45',_binary '\0','权限管理','system/permission/index',1,4,'permission','permission'),(5,'2018-12-18 15:17:28',_binary '\0','菜单管理','system/menu/index',1,5,'menu','menu'),(24,'2019-01-04 16:24:48',_binary '\0','三级菜单1','nested/menu1/menu1-1',22,999,'menu','menu1-1'),(27,'2019-01-07 17:27:32',_binary '\0','三级菜单2','nested/menu1/menu1-2',22,999,'menu','menu1-2'),(35,'2019-03-25 09:46:00',_binary '\0','部门管理','system/dept/index',1,6,'dept','dept'),(39,'2019-04-10 11:49:04',_binary '\0','字典管理','system/dict/index',1,8,'dictionary','dict'),(40,'2019-05-11 09:51:57',_binary '\0','学生管理','',0,600,'dept','student'),(41,'2019-05-11 09:55:22',_binary '\0','学生基础信息','student/base',40,999,'Steve-Jobs','base'),(42,'2019-05-11 10:01:40',_binary '\0','动态画像','',0,999,'monitor','statistics'),(43,'2019-05-11 10:03:39',_binary '\0','课程画像','statistics/curriculum',42,999,'codeConsole','curriculum'),(44,'2019-05-12 11:05:55',_binary '\0','我的学习','student/study',40,999,'Steve-Jobs','study'),(45,'2019-05-12 11:09:56',_binary '\0','学生成绩','student/achievement',40,999,'gonggao','achievement'),(46,'2019-05-12 11:18:18',_binary '\0','审核管理','',0,999,'markdown','check'),(47,'2019-05-12 11:19:00',_binary '\0','学习行为','check/study',46,999,'anq','study');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16  9:31:29
