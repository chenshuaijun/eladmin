package me.zhengjie.modules.system.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 角色
 *
 * @author jie
 * @date 2018-11-22
 */
@Entity
@Table(name = "users_roles")
@Getter
@Setter
@ToString
public class UsersRoles implements Serializable {

    @Id
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "role_id")
    private Long roleId;
}
