package me.zhengjie.modules.monitor.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.zhengjie.modules.monitor.domain.Visits;
import me.zhengjie.modules.monitor.repository.VisitsRepository;
import me.zhengjie.modules.monitor.service.VisitsService;
import me.zhengjie.modules.student.repository.XingweiRepository;
import me.zhengjie.repository.LogRepository;
import me.zhengjie.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalField;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author jie
 * @date 2018-12-13
 */
@Slf4j
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class VisitsServiceImpl implements VisitsService {

    @Autowired
    private VisitsRepository visitsRepository;

    @Autowired
    private LogRepository logRepository;

    @Override
    public void save() {
        LocalDate localDate = LocalDate.now();
        Visits visits = visitsRepository.findByDate(localDate.toString());
        if (visits == null) {
            visits = new Visits();
            visits.setWeekDay(StringUtils.getWeekDay());
            visits.setPvCounts(1L);
            visits.setIpCounts(1L);
            visits.setDate(localDate.toString());
            visitsRepository.save(visits);
        }
    }

    @Override
    public void count(HttpServletRequest request) {
        LocalDate localDate = LocalDate.now();
        Visits visits = visitsRepository.findByDate(localDate.toString());
        visits.setPvCounts(visits.getPvCounts() + 1);
        long ipCounts = logRepository.findIp(localDate.toString(), localDate.plusDays(1).toString());
        visits.setIpCounts(ipCounts);
        visitsRepository.save(visits);
    }

    @Override
    public Object get() {
        Map map = new HashMap();
        LocalDate localDate = LocalDate.now();
        Visits visits = visitsRepository.findByDate(localDate.toString());
        List<Visits> list = visitsRepository.findAllVisits(localDate.minusDays(6).toString(), localDate.plusDays(1).toString());

        long recentVisits = 0, recentIp = 0;
        for (Visits data : list) {
            recentVisits += data.getPvCounts();
            recentIp += data.getIpCounts();
        }
        map.put("newVisits", visits.getPvCounts());
        map.put("newIp", visits.getIpCounts());
        map.put("recentVisits", recentVisits);
        map.put("recentIp", recentIp);
        return map;
    }

    @Resource
    XingweiRepository xingweiRepository;

    /**
     * { key: 'study', display_name: '视频学习' },
     * { key: 'courseTest', display_name: '课程测验' },
     * { key: 'access', display_name: '访问数' },
     * { key: 'work', display_name: '作业' },
     * { key: 'exam', display_name: '考试' }
     *
     * @return
     */
    @Override
    public Object getChartData() {
        Map map = new HashMap();
        LocalDate localDate = LocalDate.now();
//        List<Visits> list = visitsRepository.findAllVisits(localDate.minusDays(6).toString(), localDate.plusDays(1).toString());

        List list = new ArrayList();
        List list1 = new ArrayList();
        List list2 = new ArrayList();
        List list3 = new ArrayList();
        List list4 = new ArrayList();
        List list5 = new ArrayList();
        for (int x = 7; x > 0; x--) {
            String date = LocalDate.now().minusDays(x).toString().substring(0, 10);
            list.add(date.substring(5));
            list1.add(xingweiRepository.countByDateAndXingwei("study", date));
            list2.add(xingweiRepository.countByDateAndXingwei("courseTest", date));
            list3.add(xingweiRepository.countByDateAndXingwei("access", date));
            list4.add(xingweiRepository.countByDateAndXingwei("work", date));
            list5.add(xingweiRepository.countByDateAndXingwei("exam", date));

        }
        map.put("weekDays", list);
        map.put("视频学习", list1);
        map.put("课程测验", list2);
        map.put("访问数", list3);
        map.put("作业", list4);
        map.put("考试", list5);
        return map;
    }


}
