package me.zhengjie.modules.student.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "xingwei")
public class Xingwei  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    private long userId;

    private String username;

    private String xingwei;

    private String xueqi;

    private String kecheng;

    private String xuexiriqi;

    private long fen;

    private String status;



}
