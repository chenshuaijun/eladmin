package me.zhengjie.modules.student.repository;

import me.zhengjie.modules.student.domain.Xuexituijian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface XuexiTuijianRepository
        extends JpaRepository<Xuexituijian, Long>, JpaSpecificationExecutor {

}
