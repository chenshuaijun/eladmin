package me.zhengjie.modules.student.repository;

import me.zhengjie.modules.student.domain.Xingwei;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * @author chenshuaijun
 */
public interface XingweiRepository
        extends JpaRepository<Xingwei, Long>, JpaSpecificationExecutor {
    /**
     * 根据用户姓名查询行为信息
     *
     * @param username
     * @param pageable
     * @return
     */
    Page<Xingwei> findByUsername(String username, Pageable pageable);

    @Query(
            value = "select count(1) from xingwei where xingwei =:xingwei and left(xuexiriqi,10)=:date",
            nativeQuery = true
    )
    int countByDateAndXingwei(@Param("xingwei") String xingwei, @Param("date") String date);


    @Query(
            value = "select count(1) from xingwei where xingwei =:xingwei and user_id=:userId",
            nativeQuery = true
    )
    int findXingweiCountInfo(@Param("userId") Long userId, @Param("xingwei") String xingwei);

    @Query(
            value = "select count(1) from xingwei where xingwei =:xingwei",
            nativeQuery = true
    )
    int findXingweiCountInfoMax(@Param("xingwei") String xingwei);

    @Query(
            value = "select avg(fen) as fen from xingwei where xingwei =:xingwei and user_id=:userId and kecheng=:kecheng and status='2'",
            nativeQuery = true
    )
    Map findXingweiFen(@Param("xingwei") String xingwei,@Param("userId") Long userId,@Param("kecheng") String kecheng);

    @Query(
            value = "select avg(chengji)  from chengji cj where kecheng =:kecheng and user_id=:userId",
            nativeQuery = true
    )
    long findXingweiCountInfoKecheng(@Param("userId") Long userId, @Param("kecheng") String kecheng);



}
