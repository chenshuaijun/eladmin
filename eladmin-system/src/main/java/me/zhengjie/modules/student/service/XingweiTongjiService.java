package me.zhengjie.modules.student.service;

import lombok.extern.slf4j.Slf4j;
import me.zhengjie.modules.student.repository.ChengjiRepository;
import me.zhengjie.modules.student.repository.XingweiRepository;
import me.zhengjie.modules.student.service.dto.ChengjiTongjiDTO;
import me.zhengjie.modules.student.service.dto.XingweiTongjiDTO;
import me.zhengjie.modules.system.domain.User;
import me.zhengjie.modules.system.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Slf4j
@Service
public class XingweiTongjiService {

    @Resource
    XingweiRepository xingweiRepository;

    @Resource
    UserRepository userRepository;
    @Resource
    ChengjiRepository chengjiRepository;

    Map<String, String> leibie = new HashMap<String, String>() {
        {
            put("1", "逻辑分析能力");
            put("2", "创新能力");
            put("3", "实践能力");
            put("4", "开发能力");
            put("5", "管理能力");
        }
    };

    public ChengjiTongjiDTO getChengjiTongji(Long userId) {
        ChengjiTongjiDTO dto = new ChengjiTongjiDTO();
        List<Map> indicator = new ArrayList<>();
        List data = new ArrayList<>();
        for (String key : leibie.keySet()) {
            Map result = chengjiRepository.findChengjiCountInfo1(userId, key);
            Map cator = new HashMap();
            cator.put("name", leibie.get(key));
            cator.put("max", result.get("max") == null ? 100 : result.get("max"));
            indicator.add(cator);
            data.add(result.get("value") == null ? 0 : result.get("value"));
        }
        dto.setIndicator(indicator);
        Map datas = new HashMap();
        datas.put("value", data);
        datas.put("name", "学习能力");
        List dataList = new ArrayList<>();
        dataList.add(datas);
        dto.setData(dataList);
        return dto;
    }

    Map<String, String> xingweis = new HashMap<String, String>() {
        {
            put("study", "视频学习");
            put("courseTest", "课程测验");
            put("access", "访问数");
            put("work", "作业");
            put("exam", "考试");
        }
    };

    public XingweiTongjiDTO getXingweiTongji(Long userId, String kecheng) {
        XingweiTongjiDTO dto = new XingweiTongjiDTO();
        List data = new ArrayList<>();
        for (String key : xingweis.keySet()) {
            Map fen = xingweiRepository.findXingweiFen(key, userId, kecheng);
            if (fen != null) {
                data.add(fen.get("fen"));
            } else {
                data.add(0);
            }
        }

        Optional<User> optionalUser = userRepository.findById(userId);
        List<String> legend = new ArrayList<>();

        Map datas = new HashMap();
        datas.put("value", data);
        datas.put("name", optionalUser.isPresent() ? optionalUser.get().getUsername() : "学习行为");
        legend.add(optionalUser.isPresent() ? optionalUser.get().getUsername() : "学习行为");

        List dataList = new ArrayList<>();
        dataList.add(datas);
        dto.setLegend(legend);
        dto.setData(dataList);
        return dto;
    }


}
