package me.zhengjie.modules.student.service.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class KechengDTO implements Serializable {

    private long id;

    private String name;

    private BigDecimal xuefen;

    private BigDecimal keshi;

    private String leibie;

    private String kaikexueyuan;
}
