package me.zhengjie.modules.student.service;

import lombok.extern.slf4j.Slf4j;
import me.zhengjie.modules.student.repository.ChengjiRepository;
import me.zhengjie.modules.student.service.dto.ChengjiTongjiDTO;
import me.zhengjie.modules.student.service.dto.IndexDTO;
import me.zhengjie.modules.system.domain.User;
import me.zhengjie.modules.system.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Slf4j
@Service
public class ChengjiTongjiService {

    @Resource
    ChengjiRepository chengjiRepository;

    Map<String, String> leibie = new HashMap<String, String>() {
        {
            put("1", "逻辑分析能力");
            put("2", "创新能力");
            put("3", "实践能力");
            put("4", "开发能力");
            put("5", "管理能力");
        }
    };
    @Resource
    UserRepository userRepository;


    public ChengjiTongjiDTO getChengjiTongji(Long userId, String xueqi) {
        ChengjiTongjiDTO dto = new ChengjiTongjiDTO();
        List<Map> indicator = new ArrayList<>();
        List data = new ArrayList<>();
        List dataMax = new ArrayList<>();
        for (String key : leibie.keySet()) {
            Map resultMax = chengjiRepository.findChengjiCountInfoMax(key, xueqi);
            dataMax.add(resultMax.get("max") == null ? 0 : resultMax.get("max"));
            Map cator = new HashMap();
            cator.put("name", leibie.get(key));
            cator.put("max", resultMax.get("max") == null ? 100 : resultMax.get("max"));

            Map result = chengjiRepository.findChengjiCountInfo(userId, key, xueqi);
            data.add(result.get("value") == null ? 0 : result.get("value"));

            indicator.add(cator);
        }
//        dto.setIndicator(indicator);

        Optional<User> optionalUser = userRepository.findById(userId);
        List<String> legend = new ArrayList<>();

        Map hashMapMax = new HashMap();
        hashMapMax.put("value", dataMax);
        hashMapMax.put("name", "最优成绩");
        legend.add("最优成绩");

        Map datas = new HashMap();
        datas.put("value", data);
        datas.put("name", optionalUser.isPresent() ? optionalUser.get().getUsername() : "学习能力");
        legend.add(optionalUser.isPresent() ? optionalUser.get().getUsername() : "学习能力");

        List dataList = new ArrayList<>();
        dataList.add(datas);
        dataList.add(hashMapMax);

        dto.setLegend(legend);

        dto.setData(dataList);

        return dto;
    }

    public Object getChengjiTongji(Long userId) {
        Map result = new HashMap() {{
            put("data1", getChengjiTongji(userId, "1"));
            put("data2", getChengjiTongji(userId, "2"));
//            put("data3", getChengjiTongji(userId, "3"));
//            put("data4", getChengjiTongji(userId, "4"));
        }};
        return result;
    }


    public ChengjiTongjiDTO getChengjiTongji1(Long userId) {
        List<Map> indicator = new ArrayList<>();
        List data = new ArrayList<>();
        for (String key : leibie.keySet()) {
            long value = 0;//chengjiRepository.findChengjiCountBykecheng(userId, key);
            data.add(value);

            Map cator = new HashMap();
            cator.put("name", leibie.get(key));
            cator.put("max", "行为分析");
        }
        Map map = new HashMap() {{
            put("value", data);
            put("name", "行为分析");
        }};
        List<String> legend = new ArrayList<>();
        legend.add("行为分析");

        List dataList = new ArrayList<>();
        dataList.add(map);
        ChengjiTongjiDTO dto = new ChengjiTongjiDTO();
        dto.setLegend(legend);
        dto.setData(dataList);
        return dto;
    }

    public IndexDTO visitsIndex() {
        IndexDTO indexDTO = new IndexDTO();
        Map maxAvgMap = chengjiRepository.findAvgChengji();
        Map maxAvgKecheng = chengjiRepository.findAvgKecheng();
        if (maxAvgMap != null) {
            indexDTO.setDiyiming(maxAvgMap.get("username"));
            indexDTO.setZuihaochengji(maxAvgMap.get("value"));
        }
        if (maxAvgKecheng != null) {
            indexDTO.setZuihaoleibie(this.leibie.get(maxAvgKecheng.get("leibie")));
            indexDTO.setZuiyoukecheng(maxAvgKecheng.get("name"));
        }
        return indexDTO;
    }
}
