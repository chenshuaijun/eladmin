package me.zhengjie.modules.student.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class XingweiDTO implements Serializable {

    private long id;

    private long userId;

    private String username;

    private String xingwei;

    private String xueqi;

    private String kecheng;

    private long fen;

    private String xuexiriqi;



}
