package me.zhengjie.modules.student.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@Entity
@Table(name = "chengji")
@NoArgsConstructor
@AllArgsConstructor
public class Chengji implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    private long userId;

    /**
     * 用户名称
     */
    private String username;

    private String kecheng;

    private String xueqi;

    private BigDecimal chengji;

    private String remark;
}
