package me.zhengjie.modules.student.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IndexDTO {
    private Object zuihaochengji;
    private Object zuihaoleibie;
    private Object zuiyoukecheng;
    private Object diyiming;
}
