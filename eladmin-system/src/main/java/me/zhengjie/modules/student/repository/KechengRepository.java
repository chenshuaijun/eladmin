package me.zhengjie.modules.student.repository;

import me.zhengjie.modules.student.domain.Kecheng;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface KechengRepository
        extends JpaRepository<Kecheng, Long>, JpaSpecificationExecutor {

    @Query(
            value = "select * from kecheng kc ",
            countQuery = "select count(1) kecheng ",
            nativeQuery = true
    )
    Page<Kecheng> findKechengInfo(Pageable pageable);

}
