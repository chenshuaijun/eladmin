package me.zhengjie.modules.student.rest;

import cn.hutool.core.bean.BeanUtil;
import me.zhengjie.aop.log.Log;
import me.zhengjie.modules.student.domain.Xuexituijian;
import me.zhengjie.modules.student.repository.XuexiTuijianRepository;
import me.zhengjie.modules.student.service.ChengjiService;
import me.zhengjie.modules.student.service.ChengjiTongjiService;
import me.zhengjie.modules.student.service.dto.ChengjiDTO;
import me.zhengjie.modules.student.service.dto.XuexiTuijianDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class ChengjiRest {
    @Resource
    ChengjiService chengjiService;
    @Resource
    ChengjiTongjiService chengjiTongjiService;

    @Log("查询用户")
    @GetMapping(value = "/chengji")
    // @PreAuthorize("hasAnyRole('ADMIN','USER_ALL','USER_SELECT')")
    public ResponseEntity getCourse(ChengjiDTO chengjiDTO, Pageable pageable) {
        return new ResponseEntity(chengjiService.getchengjiPage(chengjiDTO, pageable), HttpStatus.OK);
    }

    @Log("查询用户")
    @GetMapping(value = "/count/{userId}")
    public ResponseEntity count(@PathVariable Long userId) {
        return new ResponseEntity(chengjiTongjiService.getChengjiTongji(userId), HttpStatus.OK);
    }
    @Log("查询用户")
    @GetMapping(value = "/count1/{userId}")
    public ResponseEntity count1(@PathVariable Long userId) {
        return new ResponseEntity(chengjiTongjiService.getChengjiTongji1(userId), HttpStatus.OK);
    }


    @Log("新增成绩信息")
    @PostMapping(value = "/chengji")
    public ResponseEntity addchengji(@Validated @RequestBody ChengjiDTO chengjiDTO) {
        return new ResponseEntity(chengjiService.addchengjiPage(chengjiDTO), HttpStatus.CREATED);
    }

    @Log("修改成绩信息")
    @PutMapping(value = "/chengji")
    public ResponseEntity putchengji(@Validated @RequestBody ChengjiDTO chengjiDTO) {
        chengjiService.updatechengjiPage(chengjiDTO);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除成绩信息")
    @DeleteMapping(value = "/chengji/{id}")
    public ResponseEntity delchengji(@PathVariable Long id) {
        chengjiService.delchengji(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Resource
    XuexiTuijianRepository xuexiTuijianRepository;

    @Log("新增成绩信息")
    @PostMapping(value = "/xuexituijian")
    public ResponseEntity xuexituijian(@Validated @RequestBody XuexiTuijianDTO xuexiTuijianDTO) {
        Optional<Xuexituijian> optional = xuexiTuijianRepository.findById(xuexiTuijianDTO.getUserId());
        if (optional.isPresent()) {
            Xuexituijian xuexiTuijian = optional.get();
            xuexiTuijian.setXuexituijian(xuexiTuijianDTO.getXuexituijian());
            xuexiTuijianRepository.save(xuexiTuijian);
        } else {
            Xuexituijian xuexiTuijian = new Xuexituijian();
            BeanUtil.copyProperties(xuexiTuijianDTO, xuexiTuijian);
            xuexiTuijianRepository.save(xuexiTuijian);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @Log("查询用户")
    @GetMapping(value = "/visits-index")
    // @PreAuthorize("hasAnyRole('ADMIN','USER_ALL','USER_SELECT')")
    public ResponseEntity visitsIndex() {


        return new ResponseEntity(chengjiTongjiService.visitsIndex(), HttpStatus.OK);
    }
}
