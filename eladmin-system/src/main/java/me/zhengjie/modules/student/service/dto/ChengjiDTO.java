package me.zhengjie.modules.student.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class ChengjiDTO implements Serializable {

    private long id;
    /**
     * 用户名称
     */
    private long userId;
    /**
     * 用户名称
     */
    private String username;

    private String kecheng;

    private String xueqi;

    private BigDecimal chengji;

    private String remark;
}
