package me.zhengjie.modules.student.service;

import cn.hutool.core.bean.BeanUtil;
import me.zhengjie.modules.student.domain.Kecheng;
import me.zhengjie.modules.student.repository.KechengRepository;
import me.zhengjie.modules.student.service.dto.KechengDTO;
import me.zhengjie.utils.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class KechengService {
    @Resource
    KechengRepository kechengRepository;

    public Object getKechengPage(Pageable pageable) {
        Page<Kecheng> page = kechengRepository.findKechengInfo(pageable);
        return PageUtil.toPage(page);
    }

    public Object addKechengPage(KechengDTO kechengDTO) {
        Kecheng kecheng = new Kecheng();
        BeanUtil.copyProperties(kechengDTO, kecheng);
        return kechengRepository.save(kecheng);
    }

    public void delKecheng(Long id) {
        kechengRepository.deleteById(id);
    }

    public void updateKechengPage(KechengDTO kechengDTO) {
        Optional<Kecheng> optional = kechengRepository.findById(kechengDTO.getId());
        if (optional.isPresent()) {
            Kecheng kecheng = optional.get();
            BeanUtil.copyProperties(kechengDTO, kecheng);
            kechengRepository.save(kecheng);
        }
    }

    public Object getAllKecheng() {
        List<Kecheng> page = kechengRepository.findAll();
        return page;
    }
}
