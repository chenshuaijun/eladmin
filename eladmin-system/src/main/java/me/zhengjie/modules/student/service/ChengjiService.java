package me.zhengjie.modules.student.service;

import cn.hutool.core.bean.BeanUtil;
import me.zhengjie.modules.student.domain.Chengji;
import me.zhengjie.modules.student.repository.ChengjiRepository;
import me.zhengjie.modules.student.service.dto.ChengjiDTO;
import me.zhengjie.utils.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChengjiService {
    @Resource
    ChengjiRepository chengjiRepository;

    public Object getchengjiPage(ChengjiDTO chengjiDTO, Pageable pageable) {
        Page<Chengji> page = chengjiRepository.findAll(new Spec(chengjiDTO), pageable);
        return PageUtil.toPage(page);
    }


    class Spec implements Specification<Chengji> {
        private ChengjiDTO chengjiDTO;

        public Spec(ChengjiDTO chengjiDTO) {
            this.chengjiDTO = chengjiDTO;
        }

        @Override
        public Predicate toPredicate(Root<Chengji> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
            List<Predicate> list = new ArrayList<Predicate>();
            if (!ObjectUtils.isEmpty(chengjiDTO.getUsername())) {
                list.add(cb.like(root.get("username").as(String.class), "%" + chengjiDTO.getUsername() + "%"));
            }
            if (!ObjectUtils.isEmpty(chengjiDTO.getChengji())) {
                list.add(cb.equal(root.get("chengji").as(BigDecimal.class), chengjiDTO.getChengji()));
            }
            if (!ObjectUtils.isEmpty(chengjiDTO.getKecheng())) {
                list.add(cb.equal(root.get("kecheng").as(String.class), chengjiDTO.getKecheng()));
            }
            Predicate[] p = new Predicate[list.size()];
            return cb.and(list.toArray(p));
        }
    }

    public Object addchengjiPage(ChengjiDTO chengjiDTO) {
        Chengji chengji = new Chengji();
        BeanUtil.copyProperties(chengjiDTO, chengji);
        chengjiRepository.save(chengji);
        return null;
    }

    public void delchengji(Long id) {
        chengjiRepository.deleteById(id);
    }

    public void updatechengjiPage(ChengjiDTO chengjiDTO) {
        Optional<Chengji> optional = chengjiRepository.findById(chengjiDTO.getId());
        if (optional.isPresent()) {
            Chengji chengji = optional.get();
            BeanUtil.copyProperties(chengjiDTO, chengji);
            chengjiRepository.save(chengji);
        }
    }
}
