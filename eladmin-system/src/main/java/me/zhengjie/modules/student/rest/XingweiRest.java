package me.zhengjie.modules.student.rest;

import me.zhengjie.aop.log.Log;
import me.zhengjie.modules.student.domain.Xingwei;
import me.zhengjie.modules.student.repository.XingweiRepository;
import me.zhengjie.modules.student.service.XingweiService;
import me.zhengjie.modules.student.service.XingweiTongjiService;
import me.zhengjie.modules.student.service.dto.ChengjiTongjiDTO;
import me.zhengjie.modules.student.service.dto.XingweiDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @PreAuthorize("hasAnyRole('ADMIN','USER_ALL','USER_SELECT')")
 */
@RestController
@RequestMapping("api")
public class XingweiRest {
    @Resource
    XingweiService xingweiService;

    @Log("查询用户")
    @GetMapping(value = "/xingwei")
    public ResponseEntity getXingwei(XingweiDTO xingweiDTO, Pageable pageable) {
        return new ResponseEntity(xingweiService.getXingweiPage(xingweiDTO, pageable), HttpStatus.OK);
    }


    @Log("新增行为信息")
    @PostMapping(value = "/xingwei")
    public ResponseEntity addxingwei(@Validated @RequestBody XingweiDTO xingweiDTO) {
        return new ResponseEntity(xingweiService.addxingweiPage(xingweiDTO), HttpStatus.CREATED);
    }

    @Log("修改行为信息")
    @PutMapping(value = "/xingwei")
    public ResponseEntity putxingwei(@Validated @RequestBody XingweiDTO xingweiDTO) {
        xingweiService.updateXingwei(xingweiDTO);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Resource
    XingweiRepository repository;

    @Log("修改行为信息")
    @PutMapping(value = "/xingwei/{id}")
    public ResponseEntity updateStatus(@PathVariable Long id) {
        Optional<Xingwei> optionalXingwei = repository.findById(id);
        if (optionalXingwei.isPresent()) {
            Xingwei xingwei = optionalXingwei.get();
            xingwei.setStatus("2");
            repository.save(xingwei);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除行为信息")
    @DeleteMapping(value = "/xingwei/{id}")
    public ResponseEntity delxingwei(@PathVariable Long id) {
        xingweiService.del(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Resource
    XingweiTongjiService tongjiService;

    @Log("统计行为")
    @GetMapping(value = "/xingwei/count/{userId}/{kecheng}")
    public ResponseEntity getXingwei(@PathVariable Long userId, @PathVariable String kecheng) {
        return new ResponseEntity(tongjiService.getXingweiTongji(userId, kecheng), HttpStatus.OK);
    }
}
