package me.zhengjie.modules.student.rest;

import me.zhengjie.aop.log.Log;
import me.zhengjie.modules.student.service.KechengService;
import me.zhengjie.modules.student.service.dto.KechengDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("api")
public class KechengRest {
    @Resource
    KechengService kechengService;

    @Log("查询课程")
    @GetMapping(value = "/kecheng")
    // @PreAuthorize("hasAnyRole('ADMIN','USER_ALL','USER_SELECT')")
    public ResponseEntity getCourse(Pageable pageable) {
        return new ResponseEntity(kechengService.getKechengPage(pageable), HttpStatus.OK);
    }
    @Log("查询课程")
    @GetMapping(value = "/kecheng/all")
    public ResponseEntity getALlCourse() {
        return new ResponseEntity(kechengService.getAllKecheng(), HttpStatus.OK);
    }

    @Log("新增课程信息")
    @PostMapping(value = "/kecheng")
    public ResponseEntity addKecheng(@Validated @RequestBody KechengDTO kechengDTO) {
        return new ResponseEntity(kechengService.addKechengPage(kechengDTO), HttpStatus.CREATED);
    }

    @Log("修改课程信息")
    @PutMapping(value = "/kecheng")
    public ResponseEntity putKecheng(@Validated @RequestBody KechengDTO kechengDTO) {
        kechengService.updateKechengPage(kechengDTO);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除课程信息")
    @DeleteMapping(value = "/kecheng/{id}")
    public ResponseEntity delKecheng(@PathVariable Long id) {
        kechengService.delKecheng(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
