package me.zhengjie.modules.student.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class ChengjiTongjiDTO {

    private List<Map> indicator;

    private List data;

    private List legend;
}
