package me.zhengjie.modules.student.repository;

import me.zhengjie.modules.student.domain.Chengji;
import me.zhengjie.modules.student.domain.Kecheng;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;

import java.util.Map;

public interface ChengjiRepository
        extends JpaRepository<Chengji, Long>, JpaSpecificationExecutor {
    @Query(
            value = "select * from chengji kc ",
            countQuery = "select count(1) chengji ",
            nativeQuery = true
    )
    Page<Chengji> findChengjiInfo(Pageable pageable);

    @Query("" +
            "select avg(cj.chengji) as value ,max(cj.chengji) as max " +
            "from Chengji cj " +
            "left join Kecheng kc " +
            "on cj.kecheng=kc.id " +
            "where kc.leibie = :key and cj.userId=:userId ")
    Map findChengjiCountInfo1(@Param("userId") long userId, @Param("key") String key);

    @Query("" +
            "select avg(cj.chengji) as value ,max(cj.chengji) as max " +
            "from Chengji cj " +
            "left join Kecheng kc " +
            "on cj.kecheng=kc.id " +
            "where kc.leibie = :key and cj.userId=:userId and cj.xueqi=:xueqi")
    Map findChengjiCountInfo(@Param("userId") long userId, @Param("key") String key,@Param("xueqi") String xueqi);


    @Query("" +
            "select avg(cj.chengji) as value ,max(cj.chengji) as max " +
            "from Chengji cj " +
            "left join Kecheng kc " +
            "on cj.kecheng=kc.id " +
            "where kc.leibie = :key and cj.xueqi=:xueqi")
    Map findChengjiCountInfoMax(@Param("key") String key, @Param("xueqi") String xueqi);


    @Query(
            value = "select max(u.chengji) as value, u.username from (select avg(cj.chengji) chengji ,cj.username from chengji cj group by cj.username) u",
            nativeQuery = true
    )
    Map findAvgChengji();

    @Query(
            value = "select max(u.chengji), u.kecheng,k.name,k.leibie from (\n" +
                    "select avg(cj.chengji) chengji ,cj.kecheng from chengji cj group by cj.kecheng\n" +
                    ") as u left join kecheng k on k.id=u.kecheng",
            nativeQuery = true
    )
    Map findAvgKecheng();



//    @Query("select avg(chengji) value " +
//            "from chengji cj " +
//            "where cj.kecheng in (select id from kecheng kc where kc.leibie=:leibie) and cj.user_id = :userId")
//    long findChengjiCountBykecheng(@Param("userId") long userId, @Param("leibie") String leibie);

}
