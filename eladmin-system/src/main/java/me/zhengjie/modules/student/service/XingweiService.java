package me.zhengjie.modules.student.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import me.zhengjie.modules.student.domain.Xingwei;
import me.zhengjie.modules.student.repository.XingweiRepository;
import me.zhengjie.modules.student.service.dto.XingweiDTO;
import me.zhengjie.utils.PageUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class XingweiService {
    @Resource
    XingweiRepository xingweiRepository;

    public Object getXingweiPage(XingweiDTO xingweiDTO, Pageable pageable) {
        return PageUtil.toPage(xingweiRepository.findAll(new Spec(xingweiDTO), pageable));
    }

    class Spec implements Specification<Xingwei> {
        private XingweiDTO xingweiDTO;

        public Spec(XingweiDTO xingweiDTO) {
            this.xingweiDTO = xingweiDTO;
        }

        @Override
        public Predicate toPredicate(Root<Xingwei> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
            List<Predicate> list = new ArrayList<Predicate>();
            if (!ObjectUtils.isEmpty(xingweiDTO.getUsername())) {
                /**
                 * 模糊
                 */
                list.add(cb.like(root.get("username").as(String.class), "%" + xingweiDTO.getUsername() + "%"));
            }
            Predicate[] p = new Predicate[list.size()];
            return cb.and(list.toArray(p));
        }
    }

    public Object addxingweiPage(XingweiDTO xingweiDTO) {
        Xingwei xingwei = new Xingwei();
        BeanUtil.copyProperties(xingweiDTO, xingwei);
        return xingweiRepository.save(xingwei);
    }

    public void del(Long id) {
        xingweiRepository.deleteById(id);
    }

    public void updateXingwei(XingweiDTO xingweiDTO) {
        Optional<Xingwei> optional = xingweiRepository.findById(xingweiDTO.getId());
        if (optional.isPresent()) {
            Xingwei xingwei = optional.get();
            xingwei.setStatus("1");
            BeanUtil.copyProperties(xingweiDTO, xingwei, CopyOptions.create().ignoreNullValue());
            xingweiRepository.save(xingwei);
        }
    }
}
